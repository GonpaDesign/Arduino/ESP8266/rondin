/* 
 *  GREEN LED - READ INFORMATION  - 2 BLINKS AT READ
 *  RED LED   - LOW BATTERY       - BLINK 500ms AT 20% OR LESS
 *  
 *  NIVEL BATERIA 100%  - 4.2V  - ARead 975
 *  NIVEL BATERIA 50%   - 3.82V - ARead 886
 *  NIVEL BATERIA 20%   - 3.6V  - ARead 833
 *  NIVEL BATERIA 0%    - 3.45V - ARead 798
 */



#include <Wire.h>
#include <RtcDS3231.h>
#include <ArduinoJson.h>
#include "FS.h"
#include <ESP8266WiFi.h>
#include <OneWire.h>

#define BTN 14
#define GLED 12
#define RLED 13
#define DEBUG 0

RtcDS3231<TwoWire> rtcObject(Wire);
OneWire DS9090(0);

struct strButton{
  int pin = 10;
  int init = 0;
  int end = 0;
  bool state = false;
  bool readed = false;
};

struct strBattery{
  int lvl = 0;
  int check = 5000;
  int lastCheck = 0;
  int lastBlink = 0;
  int pulse = 500;
};

struct strLED{
  bool state = false;
  int pulse = 1500;
  int lastBlink = 0;
};

strBattery Battery;
strLED iLed;

int btnStart = 0;
int btnEnd = 0;
bool btnState = false;
int lastTick = 0;
int batteryLevel = 0;

// iButton Vars
byte addr[8];
byte lastTag[8];
byte dateTime[6];

void setup() 
{
  // Set WiFi device OFF to save power
  WiFi.mode(WIFI_OFF);
  // Starts Serial Connection
  Serial.begin(115200); 
  // Wait until ready
  while(!Serial){}
  Serial.println();
  // Starts I2C for RTC
  rtcObject.Begin();
  // RTC Initialization sequence
  rtcBegin();
  // Define button Pin as input
  pinMode(BTN, INPUT);
  // Define RED LED 
  pinMode(RLED, OUTPUT);
  digitalWrite(RLED, LOW);
  // Define GREEN LED
  pinMode(GLED, OUTPUT);
  digitalWrite(GLED, LOW);
  // Inicializamos SPIFFS
  if(!SPIFFS.begin())
  {
    if(DEBUG){Serial.println("SPIFFS Initialization...failed");}
  }
}

void loop() 
{
  // TAG Read check
  if(availableTag() && !compareArrays())
  {
    if(DEBUG){
      Serial.print("Date time: ");Serial.print(getCurrentTime());
      Serial.print(", Battery level: "); Serial.print(analogRead(A0));
      Serial.print(", Button State: "); Serial.print(digitalRead(13));
      Serial.print(",Tag Read: ");
      Serial.print(addr[7], HEX);Serial.print(" ");
      Serial.print(addr[6], HEX);Serial.print(" ");
      Serial.print(addr[5], HEX);Serial.print(" ");
      Serial.print(addr[4], HEX);Serial.print(" ");
      Serial.print(addr[3], HEX);Serial.print(" ");
      Serial.print(addr[2], HEX);Serial.print(" ");
      Serial.print(addr[1], HEX);Serial.print(" ");
      Serial.println(addr[0], HEX);  
    }
    logAppend();
    iLed.state = true;
    copyArray();
  }
  // Battery check  
  if(millis() > Battery.lastCheck + Battery.check){
    Battery.lvl = analogRead(A0);
    Battery.lastCheck = millis();
    if(DEBUG){
      Serial.print("Battery level: "); Serial.println(Battery.lvl);
    }
  }
  // Battery LED control
  if(Battery.lvl <  833)
  {
    if(millis() > (Battery.lastBlink + 2 * Battery.pulse)){
        digitalWrite(RLED,!digitalRead(RLED));
        Battery.lastBlink = millis();
    }else if(Battery.lastBlink + Battery.pulse){
      if(digitalRead(RLED)){
        digitalWrite(RLED,!digitalRead(RLED));
        Battery.lastBlink = millis();
      }
    }
  }
  // TAG Read LED Control
  if(iLed.state == true){
    if(digitalRead(GLED)){
      if(millis() > iLed.lastBlink + iLed.pulse){
        digitalWrite(GLED, LOW);
        iLed.state = false; 
      }
    }else{
      digitalWrite(GLED, HIGH);
    }
  }
  // Serial events detect
  serialRead();
  // Delay to avoid processor stack
  delay(2);
}

// ---- Funciones para Leer Serial ----

void serialRead()
{
  String incomming = "";
  
  if (Serial.available() > 0) {
    incomming = Serial.readStringUntil('\n');
  }
  if(incomming != ""){
    if(incomming.startsWith("0x1C")){
      incomming.remove(0,5);
      Serial.println(incomming);
      saveConfig(incomming);
    }else if (incomming == "0x1D"){
      eraseFile();
    }else if (incomming == "0x1E"){
      sendConfig();
    }else if (incomming == "0x1A"){
      sendFile();
    }else if (incomming == "0x1B"){
      eraseLog();
    }else{
      Serial.println(incomming);
      Serial.println("Comando no reconocido");
    }
  }
}

// ---- Funciones para Arrays de Tags ----

bool compareArrays()
{
  for(byte i = 0; i < 8; i++)
  {
    if(lastTag[i] != addr[i]){return false;}
  }
  return true;
}

void copyArray()
{
  for(byte i = 0; i < 8; i++)
  {
    lastTag[i] = addr[i];
  }
}

// ---- Funciones para RTC ----

void rtcBegin()
{
   // Define data and time 
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  
  if(DEBUG){
    char comp[20];
    sprintf(comp, "%04d/%02d/%02d,%02d:%02d:%02d", compiled.Year(), compiled.Month(), compiled.Day(), compiled.Hour(), compiled.Minute(), compiled.Second());
    Serial.println(comp);
  }

  if(!rtcObject.IsDateTimeValid())
  {
    if (rtcObject.LastError() != 0)
    {
      if(DEBUG){Serial.println(rtcObject.LastError());}
    }
    else
    {
      rtcObject.SetDateTime(compiled);
    }
  }

  if (!rtcObject.GetIsRunning())
  {
      if(DEBUG){Serial.println("RTC was not actively running, starting now");}
      rtcObject.SetIsRunning(true);
  }

  RtcDateTime now = rtcObject.GetDateTime();
  
  if (now < compiled) 
  {
      if(DEBUG){Serial.println("RTC is older than compile time!  (Updating DateTime)");}
      rtcObject.SetDateTime(compiled);
  }
  else if (now > compiled) 
  {
      if(DEBUG){Serial.println("RTC is newer than compile time. (this is expected)");}
  }
}

String getCurrentTime()
{
  // Get time from RTC
  RtcDateTime currentTime = rtcObject.GetDateTime();
  // Declare String as Char Array
  char str[20];
  // Asign Time data to char Array
  sprintf(str, "%04d/%02d/%02d,%02d:%02d:%02d", currentTime.Year(), currentTime.Month(), currentTime.Day(), currentTime.Hour(), currentTime.Minute(), currentTime.Second());
  return str;
}

// ---- Funciones del File System ----

void fsBegin()
{
  if (!SPIFFS.begin())
  {
    if(DEBUG){Serial.println("Unable to set File System");}
  }
  else
  {
    //readConfig();
  }
}

void logAppend()
{   
    File file = SPIFFS.open("/tagReads.csv", "a");
 
    if(!file){
        if(DEBUG){Serial.println("There was an error opening the file for appending");}
        return;
    }

    String data = "";
    data += getCurrentTime();
    data += ",";
    data += String(addr[0], HEX);
    data += " ";
    data += String(addr[1], HEX);
    data += " ";
    data += String(addr[2], HEX);
    data += " ";
    data += String(addr[3], HEX);
    data += " ";
    data += String(addr[4], HEX);
    data += " ";
    data += String(addr[5], HEX);
    data += " ";
    data += String(addr[6], HEX);
    data += " ";
    data += String(addr[7], HEX);
    
    if(file.println(data)){
        if(DEBUG){Serial.println("File content was appended");}
    } else {
        if(DEBUG){Serial.println("File append failed");}
    }
 
    file.close();
}

void saveConfig(String data)
{   
    File file = SPIFFS.open("/config.txt", "a");
 
    if(!file){
        if(DEBUG){Serial.println("There was an error opening the file for appending");}
        return;
    }
    
    if(file.println(data)){
        if(DEBUG){Serial.println("File content was appended");}
    } else {
        if(DEBUG){Serial.println("File append failed");}
    }
 
    file.close();
    
}

void eraseFile()
{
  File file = SPIFFS.open("/config.txt", "w");
  
  if(!file)
  {
   if(DEBUG){ Serial.println("There was an error opening the file for appending");}
    return;
  }

  file.print("");
  
  file.close();
}

void sendConfig()
{
  File file = SPIFFS.open("/config.txt", "r");

  if(!file){
      if(DEBUG){Serial.println("There was an error opening the file for appending");}
      return;
  }
  int counter = 0;
  while (file.available()) {
    Serial.print("0x0E,");Serial.println(file.readStringUntil('\n'));
    counter++;
  }
  if(DEBUG){Serial.print("Readed lines: "); Serial.print(counter); Serial.print(", File size: "); Serial.print(file.size()); Serial.println(" bytes");}

  file.close();
}

void eraseLog()
{
  File file = SPIFFS.open("/tagReads.csv", "w");
  
  if(!file)
  {
   if(DEBUG){ Serial.println("There was an error opening the file for appending");}
    return;
  }

  file.print("");
  
  file.close();
}

void sendFile()
{
  File file = SPIFFS.open("/tagReads.csv", "r");

  if(!file){
      if(DEBUG){Serial.println("There was an error opening the file for appending");}
      return;
  }
  int counter = 0;
  while (file.available()) {
    Serial.print("0x0A,");Serial.println(file.readStringUntil('\n'));
    counter++;
  }
  if(DEBUG){Serial.print("Readed lines: "); Serial.print(counter); Serial.print(", File size: "); Serial.print(file.size()); Serial.println(" bytes");}
  
  file.close();
}

// ---- Funciones para Tags ----

boolean availableTag() {
  boolean availableTag = false;
  if (DS9090.search(addr)) {
    if (OneWire::crc8(addr, 7) == addr[7]) { // testa a verificacao CRC
      if (addr[0]==1) {  // para tags iButton el primer byte siempre es 1
          availableTag = true;          
      } 
    }  
  }  
 DS9090.reset();
 return availableTag; 
}
